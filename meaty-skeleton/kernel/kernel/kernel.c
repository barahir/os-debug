#include <stdio.h>
#include <stdbool.h>

#include <kernel/descrtbl.h>
#include <kernel/timer.h>
#include <kernel/tty.h>

void kernel_main(void) {
  terminal_initialize();

  gdt_init();
  idt_init();
  timer_init(50);
}
