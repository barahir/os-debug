#include <stdio.h>

#include <kernel/isr.h>
#include <kernel/tty.h>

isr_t interrupt_handlers[256];

void register_interrupt_handler(uint8_t n, isr_t handler) {
  interrupt_handlers[n] = handler;
}

// Called by the isr_common_stub when the kernel is interrupted by an exception
void isr_handler(registers_t* regs) {
  // Handle the exceptions
  if (regs->int_no <= 31) {
    char* exceptions[] = {"Division By Zero",
			"Debug",
			"Non Maskable Interrupt",
			"Breakpoint",
			"Into Detected Overflow",
			"Out of Bounds",
			"Invalid Opcode",
			"No Coprocessor",
			"Double Fault",
			"Coprocessor Segment Overrun",
			"Bad TSS",
			"Segment Not Present",
			"Stack Fault",
			"General Protection Fault",
			"Page Fault",
			"Unknown Interrupt",
			"Coprocessor Fault",
			"Alignment Check",
			"Machine Check",
			  "Unknown"};
    printf("--------------------------------------------------------------------------------");
    printf(" /!\\ Exception %d | %s /!\\\n", regs->int_no, exceptions[regs->int_no]);
    printf("--------------------------------------------------------------------------------");
    printf(" ds               | 0x%x\n", regs->ds);
    printf(" edi              | 0x%x\n", regs->edi);
    printf(" esi              | 0x%x\n", regs->esi);
    printf(" ebp              | 0x%x\n", regs->ebp);
    printf(" ebx              | 0x%x\n", regs->ebx);
    printf(" edx              | 0x%x\n", regs->edx);
    printf(" ecx              | 0x%x\n", regs->ecx);
    printf(" eax              | 0x%x\n", regs->eax);
    printf(" eip              | 0x%x\n", regs->eip);
    printf(" cs               | 0x%x\n", regs->cs);
    printf(" eflags           | 0x%x\n", regs->eflags);
    printf(" esp              | 0x%x\n", regs->esp);
    printf(" ss               | 0x%x\n", regs->ss);
    printf("--------------------------------------------------------------------------------");
    if (interrupt_handlers[regs->int_no] != 0) {
      isr_t handler = interrupt_handlers[regs->int_no];
      handler(regs);
    }
  }
}

// Called by irq_common_stub when the kernel is interrupted by an IRQ
void irq_handler(registers_t* regs) {
  // Handle the IRQs
  
  // Send an EOI
  if (regs->int_no >= 40) {
    // Send the EOI to the slave PIC
    outb(0xA0, 0x20);
  }
  // Send the EOI to the master PIC
  outb(0x20, 0x20);

  if (interrupt_handlers[regs->int_no] != 0) {
    isr_t handler = interrupt_handlers[regs->int_no];
    handler(regs);
  }
}
