#include <stdio.h>
#include <stddef.h>

#include <kernel/isr.h>
#include <kernel/timer.h>

#define PIT_CHAN_0_DATA_PORT 0x40
#define PIT_CHAN_1_DATA_PORT 0x41
#define PIT_CHAN_2_DATA_PORT 0x42
#define PIT_CMD_PORT         0x43

uint32_t tick = 0;

static void timer_callback(registers_t* regs) {
  tick++;
  printf("%d\n", tick);
}

void sleep(size_t time) {
  size_t start = tick;
  while (tick < start + time) {;}
}

void timer_init(uint32_t frequency) {
  // Register timer_callback as the interrupt handler for IRQ0
  for (int irq = IRQ0; irq <= IRQ15; irq++) {
    register_interrupt_handler(irq, &timer_callback);
  }
  
  uint32_t divisor = 1193180 / frequency;

  // Set the PIT in repeating mode and prepare it for initialization
  outb(PIT_CMD_PORT, 0x36);

  // Send the frequency divisor
  uint8_t lo = (uint8_t) (divisor & 0xFF);
  uint8_t hi = (uint8_t) ((divisor >> 8) & 0xFF);
  outb(PIT_CHAN_0_DATA_PORT, lo);
  outb(PIT_CHAN_0_DATA_PORT, hi);
}

  
