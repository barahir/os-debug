#ifndef _STDIO_H
#define _STDIO_H 1

#include <stdint.h>

#include <sys/cdefs.h>

#define EOF (-1)

#ifdef __cplusplus
extern "C" {
#endif

  int printf(const char* __restrict, ...);
  int putchar(int);
  int puts(const char*);
  
  void outb(uint16_t port,
	    uint8_t value);
  uint8_t inb(uint16_t port);
  uint16_t inw(uint16_t port);
  void io_wait(void);
  
#ifdef __cplusplus
}
#endif

#endif
