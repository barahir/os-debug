#include <limits.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include <kernel/descrtbl.h>

typedef struct word {
  const char* data;
  size_t size;
} word_t;

static bool print(word_t word) {
  const unsigned char* bytes = (const unsigned char*) word.data;
  for (size_t i = 0; i < word.size; i++)
    if (putchar(bytes[i]) == EOF)
      return false;
  return true;
}

static word_t word_of_string(const char* str) {
  word_t word;
  word.data = str;
  word.size = strlen(str);
  return word;
}

static word_t word_of_int(uint8_t base, int n) {
  if (n == 0) {
    return word_of_string("0");
  } else {
    size_t len = 0;
    int m = n;
    while (m > 0) {
      m /= base;
      len ++;
    }
    char digits[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    char data[len];
    for(int i = len - 1; i >= 0; i--) {
      data[i] = digits[n % base];
      n /= base;
    }
    word_t word;
    word.data = (const char*) data;
    word.size = len;
    return word;
  }
}

int printf(const char* __restrict__ format, ...) {
  va_list parameters;
  va_start(parameters, format);

  int written = 0;

  while (*format != '\0') {
    word_t word;
    size_t maxrem = INT_MAX - written;
    
    if (format[0] != '%' || format[1] == '%') {
      if (format[0] == '%')
	format++;
      size_t amount = 1;
      while (format[amount] && format[amount] != '%')
	amount++;
      if (maxrem < amount) {
        isr4(); // Into Detected Overflow Exception
	return -1;
      }
      word.data = format;
      word.size = amount;
      if (!print(word))
	return -1;
      format += amount;
      written += amount;
      continue;
    }

    const char* format_begun_at = format++;
    switch(*format) {
    case 's':
      format++;
      word = word_of_string(va_arg(parameters, const char*));
      break;
    case 'x':
      format++;
      word = word_of_int(16, va_arg(parameters, int));
      break;
    case 'd':
      format++;
      word = word_of_int(10, va_arg(parameters, int));
      break;
    case 'b':
      format++;
      word = word_of_int(2, va_arg(parameters, int));
      break;
    default:
      format = format_begun_at;
      word = word_of_string(format);
      format += word.size;
    }
    if (maxrem < word.size) {
        isr4(); // Into Detected Overflow Exception
      return -1;
    }
    if (!print(word)) {
      return -1;
    }
    written += word.size;
    continue;
  }

  va_end(parameters);
  return written;
}
