#include <stdint.h>

#include <stdio.h>

uint16_t inw(uint16_t port) {
  uint16_t ret;
  __asm__ __volatile__("inw %1, %0" : "=a" (ret) : "dN" (port));
  return ret;
}
