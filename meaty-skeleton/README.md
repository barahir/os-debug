# Structure
The ```kernel``` contains the files that make the kernel. The ```libc``` directory contains the C library, referenced by the C files in the kernel (```kernel.c``` for instance).
Each of these directories contains an ```arch``` directory to separate the files for different architectures. Since for now we only support the i686 architecture, it always contains a single directory.m
Finally the ```include``` directories contain the ```.h``` files.

# Building

Run ```build.sh``` to build the kernel (it will appear in ```sysroot/boot/```). If you want an ```.iso``` file, use ```iso.sh``` instead.
Similarly, running ```clean.sh``` will clean the directory of the built files.

# Running

Run ```qemu.sh``` to launch the OS in QEMU. 

# Debugging

Running ```qemu.sh``` with the ```-d``` (```--debug```) option will launch the OS in QEMU and an instance of gdb. Here's an example of what you can then do :
```
$ ./qemu.sh -d
(gdb) break _start
(gdb) continue
(gdb) layout reg
(gdb) step
```
