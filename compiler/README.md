# i686-elf-gcc

i686-elf-gcc is a version of the gcc compiler that compiles code for the i686-elf architecture (the one supported by the OS).

## Requirements

The following packages are required for the compiler to be built :
- cURL
- Tar
- Make
- Bison
- Flex
- GMP
- MPFR
- MPC
- Texinfo

See https://wiki.osdev.org/GCC_Cross-Compiler#Preparing_for_the_build for details on how to install those.

## Building

To install it, simply run ```build.sh```. If you want to change the versions of gcc and binutils used (here gcc-8.3.0 and binutils-2.32), you will have to change the first few lines of ```build.sh``` accordingly.

## Using

If you ran ```build.sh``` from the ```compiler``` directory, run ```compiler/bin/i686-elf-gcc --version```  from the root directory (```.../os-debug/```) to show the current version of your gcc cross-compiler.
Several other useful binaries can be found in ```compiler/bin/```.
